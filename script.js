/*
  Реалізувати функцію фільтру масиву за вказаним типом даних. 
  Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:

- Написати функцію filterBy(), яка прийматиме 2 аргументи. 
  Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
- Функція повинна повернути новий масив, який міститиме всі дані, які були передані 
  в аргумент, за винятком тих, тип яких був переданий другим аргументом. 
  Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', 
  то функція поверне масив [23, null].
*/
"use strict";

const checkTypeOf = (variable) =>
  variable === null ? "null" : typeof variable;

// using filter
function filterBy(arrayToFilter, dataTypeToExclude) {
  return arrayToFilter.filter(
    (item) => checkTypeOf(item) !== dataTypeToExclude
  );
}

// using forEach
function filterByVersion2(arrayToFilter, dataTypeToExclude) {
  const filtered = [];
  arrayToFilter.forEach((item) => {
    if (checkTypeOf(item) !== dataTypeToExclude) filtered.push(item);
  });
  return filtered;
}

// Test Cases
const testArray = [
  1,
  null,
  undefined,
  NaN,
  true,
  false,
  [1, 2, 3],
  "1",
  "some-string",
  { a: 1, b: "some-string" },
  Symbol("asdas"),
];

const testDataTypes = [
  "null",
  "object",
  "boolean",
  "string",
  "number",
  "symbol",
  "undefined",
];
const testCases = () => {
  console.log(testArray);
  testDataTypes.forEach((dataType) => {
    console.log(dataType);
    console.log(filterBy(testArray, dataType));
  });
};
testCases();
